import React from 'react';
import {Link} from "react-router-dom"

import "./banner_layout.scss"
import { convertToHOST } from '../../../helpers/convert_helper';

const Banner = (background,path,title) => {
    return (
        <div className="page-title">
        <div className="main-top" style={{ backgroundImage: background ? `url(${convertToHOST(background)})` : 'url("http://v4s2d7i6.stackpathcdn.com/demo-3/wp-content/uploads/sites/5/2018/10/test-header-image-3.jpg")', backgroundPosition: '0px 50px' }}>
            <div className="overlay-top-header" ></div>
            <div className="content container">
                <Link to={path} title={title}><h1 className="text-uppercase">{title}</h1></Link>
                <div className="breadcrumb-content">
                    <div className="breadcrumbs-wrapper">
                        <div className="container">
                            {/* <ul itemProp="breadcrumb" itemType="http://schema.org/BreadcrumbList" id="breadcrumbs" className="breadcrumbs">
                                <li itemProp="itemListElement" itemType="http://schema.org/ListItem">
                                    <Link itemProp="item" to={path.redirect_to_home()} title="Home">
                                        <span itemProp="name">Home</span></Link><span className="breadcrum-icon">/</span>
                                </li>
                                <li itemProp="itemListElement">
                                    <Link itemProp="name" to={path.redirect_to_story()} title={title}><span itemProp="name">{title}</span></Link>
                                </li>
                            </ul> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
};

export default Banner;