import React from 'react'
import banner from "../../../../assets/images/banner.png"

const HomeBanner = () => {
    return <div className="banner pb-3">
        <div className="container">
            <img src={banner} className="img-fluid" alt="banner" />
        </div>
    </div>
}

export default HomeBanner
