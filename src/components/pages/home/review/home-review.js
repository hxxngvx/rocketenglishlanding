import React from 'react'
import Slider from "react-slick";


const HomeReview = () => {
    return <div className="container pb-3">
        <h3 className="title text-center my-4">Các Học Viên Thường Nói: Eng Breaking Thay Đổi Cuộc Đời Tôi!</h3>
        <Slider {...settings} className="mb-5">
            <img src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2018/12/24.jpg" className="w-100" alt="#" />
            <img src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2018/12/21.jpg" className="w-100" alt="#" />
            <img src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2018/12/22.jpg" className="w-100" alt="#" />
            <img src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2018/12/22.jpg" className="w-100" alt="#" />
        </Slider>
        <h3 className="title text-center my-4">Áp dụng 3 Kỹ Thuật Thực Hành Tiếng Anh Hiệu Quả Nhất Hiện Nay</h3>
        <div className="row text-center row-text">
            <div className="col-lg-3 col-sm-6 col-12 mb-4">
                <div className="image"><img className="w-50" src="https://cdn.engbreaking.com/wp-content/uploads/2018/12/Nghe-ngam-1.jpg" /></div>
                <div className="content">
                    <h5 className="row-text-title mb-4 mt-4">Kỹ Thuật Nghe Ngấm</h5>
                    <p>
                        Nghe chủ động, <strong>từ tốc độ chậm tới nhanh</strong> để thấm <strong>cách phát âm, ngữ điệu cả câu</strong>  và hiểu được người bản xứ đang nói gì.
                        </p>
                </div>
            </div>
            <div className="col-lg-3 col-sm-6 col-12 mb-4">
                <div className="image"><img className="w-50" src="https://cdn.engbreaking.com/wp-content/uploads/2018/12/noi-duoi-1.jpg" /></div>
                <div className="content">
                    <h5 className="row-text-title mb-4 mt-4">Kỹ Thuật Nói Đuổi</h5>
                    <p>
                        Nghe chủ động, <strong>từ tốc độ chậm tới nhanh</strong> để thấm <strong>cách phát âm, ngữ điệu cả câu</strong>  và hiểu được người bản xứ đang nói gì.
                        </p>
                </div>
            </div>
            <div className="col-lg-3 col-sm-6 col-12 mb-4">
                <div className="image"><img className="w-50" src="https://cdn.engbreaking.com/wp-content/uploads/2018/12/phan-xa-da-chieu-1.jpg" /></div>
                <div className="content">
                    <h5 className="row-text-title mb-4 mt-4">Kỹ Thuật Phản Xạ Đa Chiều</h5>
                    <p>
                        Nghe chủ động, <strong>từ tốc độ chậm tới nhanh</strong> để thấm <strong>cách phát âm, ngữ điệu cả câu</strong>  và hiểu được người bản xứ đang nói gì.
                        </p>
                </div>
            </div>
            <div className="col-lg-3 col-sm-6 col-12 mb-4">
                <div className="image"><img className="w-50" src="https://cdn.engbreaking.com/wp-content/uploads/2018/12/Nghe-ngam-1.jpg" /></div>
                <div className="content">
                    <h5 className="row-text-title mb-4 mt-4">Kỹ Thuật Nghe Ngấm</h5>
                    <p>
                        Nghe chủ động, <strong>từ tốc độ chậm tới nhanh</strong> để thấm <strong>cách phát âm, ngữ điệu cả câu</strong>  và hiểu được người bản xứ đang nói gì.
                        </p>
                </div>
            </div>
        </div>
    </div>
}

const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    centerPadding: '150px',
    autoplaySpeed: 3000,
    arrows: false,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
};

export default HomeReview
