import * as React from 'react';
import Banner from '../component/banner';
import Order from '../component/order';
import Article from '../component/article';
import Feedback from '../component/feedback';
import Introduction from '../component/introduction';
import Feature from '../component/feature';
import RegisterForm from '../component/register-form';
import ImgCenter from '../component/img-center';
import feature from './feature.png';
import introduction from './feature.png';
class NewHome extends React.Component {
    render() {
        return (
            <div style={{ fontFamily: 'Montserrat' }}>
                <Banner />
                <Order />
                <Article />
                <Order />
                <Feedback />
                <Introduction />
                <ImgCenter src={introduction} />
                <Order />
                <ImgCenter src={feature} />
                <Feature />
                <Order />
                <RegisterForm />
            </div>
        )
    }
}
export default NewHome;