import React from 'react'

const HomeVideo = () => {
    return <div className="container pb-3">
        <h3 className="title text-center my-4">Với Eng Breaking Bạn Chỉ Có Thể Thành Công</h3>
        <div className="content-text">
            <iframe width="100%" height={400} src="https://www.youtube.com/embed/qNx5eHTzIFM" frameBorder={0} allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen />
        </div>
        <div className="row text-center row-text mt-5">
            <div className="col-lg-4 col-12 mb-4">
                <div className="image"><img className="w-50" src="https://cdn.engbreaking.com/wp-content/uploads/2019/01/onlineTK.png" /></div>
                <div className="content">
                    <h5 className="row-text-title mb-4 mt-4">Tài Khoản Học Online</h5>
                    <p>
                        Bạn có thể <strong>dễ dàng truy cập hoặc lưu vào</strong> điện thoại, tablet, ipad, ... để mang theo và luyện tập bất cứ đâu: phòng gym, ô tô, khi chạy bộ, ...
                </p>
                </div>
            </div>
            <div className="col-lg-4 col-12 mb-4">
                <div className="image"><img className="w-50" src="https://cdn.engbreaking.com/wp-content/uploads/2019/01/sachgiaotrinh.png" /></div>
                <div className="content">
                    <h5 className="row-text-title mb-4 mt-4">DVD Offline Và Giáo Trình In Màu</h5>
                    <p>
                        Được thiết kế bắt mắt với đầy đủ
                    <strong> nội dung bài học</strong> giúp bạn
                    <strong>dễ dàng ôn tập lại kiến thức bất cứ lúc nào</strong> ,
                                     kể cả khi bạn không thể kết nối mạng Internet.
            </p>
                </div>
            </div>
            <div className="col-lg-4 col-12 mb-4">
                <div className="image"><img className="w-50" src="https://cdn.engbreaking.com/wp-content/uploads/2019/01/sotaykehoach.png" /></div>
                <div className="content">
                    <h5 className="row-text-title mb-4 mt-4">Kế Hoạch Hành Động & Mục Tiêu</h5>
                    <p>
                        Lên sẵn 1 kế hoạch học tiếng Anh
                <strong> chi tiết tới từng phút một trong 72 ngày</strong>,
                 giúp ghi nhận lại hành trình từng ngày <strong>chinh phục mục tiêu học tiếng Anh của bạn.</strong>
                    </p>
                </div>
            </div>
        </div>
    </div>

}

export default HomeVideo
