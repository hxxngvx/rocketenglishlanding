import React from 'react';
import banner from "./banner.png";
import './index.css';
const HomeBanner = () => {
    return (
        <div className="banner-container">
            <div className="container">
                <img src={banner} className="img-fluid" alt="banner" />
            </div>
        </div>
    )
}

export default HomeBanner