import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom"
import { redirect_to_story_detail } from "../../../config/router.config"
import * as path from "../../../config/router.config"
import * as API_STORY from "../../../api/story/story_api"
import * as API_HEADER from "../../../api/header/header_api"
import { HOST_STATIC } from "../../../config/linkAPI.config"
import { returnTitle, convertDate } from "../../../helpers/convert_helper"
import Loading from "../../../helpers/loading_helper"
import Banner from "../../layout/banner/banner_layout"
import "./story_page.scss"

const StoryPage = (props) => {
    const [pageName, setPageName] = useState()
    const [arrStories, setArrStories] = useState([])
    const [loading, setLoading] = useState(true)
    const getMenuByMetatitle = async () => {
        try {
            const menu = await API_HEADER.getMenuByMetaTitle(props.match.path.split('/')[1]).catch(err => console.log(err))
            const post = await API_STORY.getPostByMenu(menu.id).catch(err => console.log(err))
            setPageName(menu.name)
            console.log(post)
            post && setArrStories(post)
            post && setLoading(false)
        } catch (err) {
            console.log(err)
        }
    }


    useEffect(() => {
        getMenuByMetatitle()
    }, [])

    const printArrStories = arrStories => {
        return arrStories.map(story => {
            return <div className="col-lg-4 col-sm-6 col-12 mb-5">
                <Link to={redirect_to_story_detail(story.metaTile)} title={story.name} className="content-each-box">
                    <div className="thumbnail-each-box">
                        <div>
                            <img className="w-100" title={story.name} src={`${HOST_STATIC}${story.thumbnail}`} alt={story.thumbnail} />
                        </div>
                        <div className="success-stories">{pageName}</div>
                    </div>
                    <div className="title-each-story">{returnTitle(story.name)}</div>
                    <div className="footer-each-box">
                        <span className="span-author">
                            <span className="author-name-each-box">{story.createdBy ? story.createBy : 'Admin'}</span>
                            <span className="space-span"> - </span>
                        </span>
                        <span className="span-post-date"> {convertDate(story.createdDate)}</span>
                    </div>
                </Link>
            </div>
        })
    }


    const printRecentStory = data => {
        return (
            <div className="item-right mb-lg-5 ">
                <h4 className="title mb-4">BÀI VIẾT KHÁC</h4>
                {
                    data.map(story =>
                        <div className="list-post mb-5 mt-3">
                            <Link to={redirect_to_story_detail(story.metaTile)} class="row">
                                <div className="col-lg-5 col-4 ">
                                    <img className="w-100" title={story.name} src={`${HOST_STATIC}${story.thumbnail}`} alt={story.thumbnail} />
                                </div>
                                <div className="col-lg-7 col-8 post-item">
                                    <a>
                                        <div className="item">
                                            <div className="title-post">{returnTitle(story.name)}</div>
                                            {/* <div className="price">$49.00
                            <span className="old-price"> $65.00</span>
                                            </div> */}
                                        </div>
                                    </a>
                                </div>
                            </Link>
                        </div>
                    )

                }
            </div>
        )
    }


    const printArrCategories = data => {
        return (
            <div className="item-right mb-lg-5">
                <h4 className="title mb-4">Danh mục</h4>
                <ul className="ul-categories">
                    {
                        data.map(item =>
                            <li className="mb-4">
                                <span className="name">{item}</span>
                            </li>)
                    }
                </ul>
            </div>
        )
    }



    return (
        <React.Fragment>
            {Loading(loading)}
            {Banner('', path.redirect_to_story, "CÂU CHUYỆN THÀNH CÔNG")}
            <div className="story-container mt-5 mb-5">
                <div className="container">
                    {/* <p className="stories-title pb-5">CÂU CHUYỆN THÀNH CÔNG</p> */}
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="row mt-5">
                                {
                                    arrStories && printArrStories(arrStories)
                                }
                            </div>
                            <div className="pagination">
                                <a href="#" className="pagination-text">«</a>
                                <a href="#" className="pagination-text">1</a>
                                <a href="#" className="pagination-text">»</a>
                            </div>
                        </div>
                        {/* <div className="col-lg-3 offset-lg-1 col-12">
                            {arrStories && printRecentStory(arrStories)} 
                        </div> */}
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default StoryPage;