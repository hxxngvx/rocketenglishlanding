import React, { useState, useEffect, useReducer } from 'react';
import { Redirect, Link } from "react-router-dom"
import "./tips_detail_page.scss"
import { redirect_to_story_detail, redirect_to_order } from "../../../../config/router.config"
import * as path from "../../../../config/router.config"
import * as API_STORY from "../../../../api/story/story_api"
import { HOST_STATIC } from "../../../../config/linkAPI.config"
import { returnTitle, convertDate } from "../../../../helpers/convert_helper"
import Loading from "../../../../helpers/loading_helper"
import Banner from '../../../layout/banner/banner_layout';

const TipsDetailPage = (props) => {

    const [detailPost, setDetailPost] = useState()
    const [loading, setLoading] = useState(true)

    const getPostDetail = async () => {
        try {
            const detailPost = await API_STORY.getPostByMetaTitle(props.match.params.metaTitle).catch(err => console.log(err))
            detailPost && setDetailPost(detailPost)
            detailPost && setLoading(false)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getPostDetail()
        return () => { };
    }, [])

    const [infoComment, setInfoComment] = useReducer((state, newState) => ({ ...state, ...newState }), {});
    const postCommemt = () => {
        if (infoComment != null) alert("Nhap thong tin")
    }

    const handleChange = e => {
        const { name, value } = e.target
        setInfoComment({ [name]: value });
    }

    const printDetailPost = detailPost => {
        return <div className="story-detail">
            <div className="content mb-3 mt-3">
                {/* <h3 className="text-center">{detailPost.name}</h3> */}
                <div dangerouslySetInnerHTML={{ __html: detailPost.content }}></div>
            </div>

            <div className="footer-story-detail">
                <div className="image text-center m-auto">
                    <img className="d" src="https://cdn-engbreaking.cdn.vccloud.vn/wp-content/uploads/2017/09/9-14-2017-10-16-30-AM.png"  />
                </div>
                <Link to={redirect_to_order()} className="btn-main link-btn-main mt-3">Tìm hiểu Clevertube Ngay >></Link>
            </div>
            {/* <div className="form-contact mt-5">
                <h6 className="form-title mb-3 mt-3">LEAVE A REPLY</h6>
                <form className="form-group">
                    <textarea onChange={e => handleChange(e)} name="comment" className="form-control mb-3" cols={20} rows={10} placeholder="Comment:" />
                    <input onChange={e => handleChange(e)} name="name" type="text" className="form-control mb-3" placeholder="Name: *" required />
                    <input onChange={e => handleChange(e)} name="email" type="email" className="form-control mb-3" placeholder="Email: *" required />
                    <input onChange={e => handleChange(e)} name="website" type="text" className="form-control mb-3" placeholder="Website: *" required />
                    <button onClick={postCommemt} className="btn-main">Post Comment</button>
                </form>
            </div> */}
        </div>
    }


    return (
        <React.Fragment>
            {Loading(loading)}
            {detailPost && Banner('',path.redirect_to_tips_detail(props.match.params.metaTitle),detailPost.name)}
            <div className="story-detail-container mt-5 mb-5">
                <div className="container">
                    {
                        detailPost && printDetailPost(detailPost)
                    }
                </div>
            </div>
        </React.Fragment>

    );
};
export default TipsDetailPage;