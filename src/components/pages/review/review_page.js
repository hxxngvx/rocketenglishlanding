import React from 'react';
import './review_page.scss';
import IntroduceReviewFaq from "../../layout/introduce_review_faq/introduce_review_faq_layout"

const ReviewPage = () => {
    return (
        <div className="review-container mt-5 mb-5">
            <div className="container">
                <h1>Reviews – Phản hồi của học viên Eng Breaking</h1>
                <div className="intro-box">
                    <p><strong>Eng Breaking</strong> tuyệt vời như thế nào?</p>
                    <p>Các học viên của Eng Breaking đang chia sẻ điều gì?</p>
                    <p>Hãy cùng theo dõi những phản hồi chi tiết và đầy thú vị từ những khách hàng của chúng tôi.</p>
                </div>
                <img src={require('../../../assets/images/review-people.png')} className="w-100" alt="" />
                <div className="list-comment mt-5">
                    <div className="comment-box mt-3 mb-3">
                        <p className="subject-each-comment">Em đã tiến bộ lên rất nhiều</p>
                        <p className="content-each-comment">Cảm ơn thầy Mike và các cộng sự của thầy . Sau 3 tháng đồng hành cùng Eng
                          Breaking em đã tiến bộ lên rất nhiều . Em mới nói chuyện với 2 người tây và họ nói phát âm của em rất
            tốt và tự nhiên .Tất cả là nhờ công của thầy . Em cảm ơn rất nhiều ạ 😘😘😘😘</p>
                        <p className="subject-each-comment">Bình - lytrabinh***@gmail.com</p>

                    </div>
                </div>


                {IntroduceReviewFaq('')}
            </div>
        </div>
    );

};

export default ReviewPage;