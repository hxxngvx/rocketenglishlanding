import React from 'react';
import "./faq_page.scss"
import IntroduceReviewFaq from "../../layout/introduce_review_faq/introduce_review_faq_layout"

const FaqPage = () => {

    return (
        <div className="faq-container mt-5 mb-5">
            <div className="container">
                <p className="faq-title">FAQ – Câu hỏi thường gặp</p>
                <div className="list-faq mt-3">
                    <div className="faq-item mb-5 mt-5">
                        <div className="question">
                            <h3>
                                1. Cho mình hỏi, bộ tài liệu này dành cho học viên ở trình độ nào? Mình gần như chưa biết gì về tiếng Anh có học được không?
                        </h3>
                        </div>
                        <div className="answer">
                            <p>
                                Trọn bộ hội thoại của Eng Breaking là do người Mỹ thiết kế và lồng tiếng 100%.
        Phương pháp Eng Breaking là một phương pháp học tiếng anh hiệu quả theo quy trình tự nhiên nhất với mục tiêu: “Nói tiếng Anh lưu loát, Dễ dàng và Tự nhiên.”
        Eng Breaking tập trung nhiều vào kỹ năng nghe – nói tiếng Anh và thực hành phản xạ. Khi bạn nghe tiếng Anh trở nên quen thuộc, bạn sẽ nói ra một cách tự tin và trôi chảy hơn.
                        </p>
                        </div>
                    </div>
                    <div className="faq-item mb-5 mt-5">
                        <div className="question">
                            <h3>2. Bạn ơi cho mình hỏi là: các đoạn hội thoại trong bài học là giọng của người nước ngoài hay người Việt?</h3>
                        </div>
                        <div className="answer">
                            <p>
                                Trọn bộ hội thoại của Eng Breaking là do người Mỹ thiết kế và lồng tiếng 100%.
                                        Phương pháp Eng Breaking là một phương pháp học tiếng anh hiệu quả theo quy trình tự nhiên nhất với mục tiêu: “Nói tiếng Anh lưu loát, Dễ dàng và Tự nhiên.”
                                        Eng Breaking tập trung nhiều vào kỹ năng nghe – nói tiếng Anh và thực hành phản xạ. Khi bạn nghe tiếng Anh trở nên quen thuộc, bạn sẽ nói ra một cách tự tin và trôi chảy hơn.
                        </p>
                        </div>
                    </div>
                    <div className="faq-item mb-5 mt-5">
                        <div className="question">
                            <h3> 3. Cho mình hỏi ngoài bộ đĩa ra thì bộ tài liệu Eng Breaking có sách kèm theo không? </h3>
                        </div>
                        <div className="answer">
                            <p> Ngoài bộ đĩa VCD thì Eng Breaking còn có sách giáo trình tiếng Anh chi tiết các bài học cùng cuốn sổ tay hành động giúp việc học của bạn trở nên dễ dàng hơn. Tất cả tài liệu này sẽ được gửi cùng một lúc khi bạn đặt hàng.
Click vào đây để Khám phá Trọn bộ Eng Breaking bạn sẽ nhận được khi mua hàng: Eng Breaking – “Đập Hộp” Chương Trình Tiếng Anh Giao Tiếp Cơ Bản Sau 3 Tháng </p>
                        </div>
                    </div>
                </div>
                {IntroduceReviewFaq('')}
            </div>
        </div>
    );
};

export default FaqPage;