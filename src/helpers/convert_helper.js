import { HOST_STATIC } from "../config/linkAPI.config"

export const convertDate = stringDate => {
    return new Date(stringDate).toDateString().substring(4, 15)
}

export const returnTitle = title => {
    let result;
    if (title.length > 65) result = title.substring(0, 65) + " ..."
    else result = title;
    return result;
}

export const convertToHOST = (string) => {
    return `${HOST_STATIC}${string}`;
}
