import React, { useState, useEffect,useReducer } from 'react';

const useStateCallBack = (initialState ,callback) => {
    // const [state, setState] = useState(initialState);

    const [state, setState] = useReducer((initialState, newState) => ({ ...initialState, ...newState }), {});


    useEffect(() => callback(state), [state, callback]);

    return [state, setState];
};


export default useStateCallBack;