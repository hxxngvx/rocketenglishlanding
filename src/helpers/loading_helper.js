import React from 'react';
import { BounceLoader } from 'react-spinners'
import { css } from '@emotion/core';
const override = css`
    display: block;
    margin: 0 auto;
    position: absolute;
    top:50%;
    left:50%;
    transform:translate(-50%,-50%)
`;

const Loading = (state) => {
    return <div className="over-loading" style={{ background: 'white', display: state ? 'block' : 'none', position: 'fixed', width: '100%', height: '100%', top: '0', zIndex: '999989999999' }}>
        <BounceLoader
            css={override}
            sizeUnit={"px"}
            size={150}
            color={'#0b3e71'}
            loading={state}
        />
    </div>
}

export default Loading;