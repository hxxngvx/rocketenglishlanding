import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
class ScrollToTopRoute extends Component {
  componentDidUpdate(prevProps) {
    console.log("hi")
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0)
    }
    const collapse = document.getElementById("collapsibleNavId")
    collapse.classList.remove('show')
  }

  render() {
    const { component: Component, ...rest } = this.props;
    return <Route {...rest} render={props => (<Component {...props} />)} />;
  }
}

export default withRouter(ScrollToTopRoute);

